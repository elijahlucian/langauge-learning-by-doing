defmodule Maff do
  def uvxy(u, v) do
    {u * :math.pi() * 2, v * :math.pi() * 2}
  end

  def zero?(n) when n == 0, do: true
  def zero?(n) when is_integer(n), do: false
end

{x, y} = Maff.uvxy(0.3, 0.43)

z = Maff.zero?(0)

IO.puts("x: #{x}, y: #{y}, z: #{z}")

f = &Maff.zero?/1

IO.puts(f.(1))

defmodule Recursion do
  def print_n_times(msg, n) when n >= 1 do
    IO.puts("msg ##{n}: #{msg}")
    print_n_times(msg, n - 1)
  end

  def print_n_times(_, _) do
  end
end

Recursion.print_n_times("asdf", 20)

case File.read("asdf") do
  {:ok, body} -> IO.puts(body)
  {:error, reason} -> IO.puts("can't read file #{reason}")
end

defmodule User do
  defstruct name: "John", age: 30
end

IO.puts(%User{}.name)
