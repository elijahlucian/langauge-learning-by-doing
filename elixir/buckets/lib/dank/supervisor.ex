defmodule Dank.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(:ok) do
    children = [
      {DynamicSupervisor, name: Dank.BucketSupervisor, strategy: :one_for_one},
      {Dank.Registry, name: Dank.Registry}
    ]

    Supervisor.init(children, strategy: :one_for_all)
  end
end
