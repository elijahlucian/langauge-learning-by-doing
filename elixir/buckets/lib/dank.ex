defmodule Dank do
  use Application

  @impl true
  def start(_type, _args) do
    Dank.Supervisor.start_link(name: Dank.Supervisor)
  end
end
