defmodule Dank.BucketTest do
  use ExUnit.Case, async: true

  setup do
    {:ok, bucket} = Dank.Bucket.start_link([])
    %{bucket: bucket}
  end

  test "stores values by key", %{bucket: bucket} do
    assert Dank.Bucket.get(bucket, "milk") == nil

    Dank.Bucket.put(bucket, "milk", 3)
    assert Dank.Bucket.get(bucket, "milk") == 3

    assert Dank.Bucket.delete(bucket, "milk") == 3
    assert Dank.Bucket.get(bucket, "milk") == nil
  end

  test "are temporary workers" do
    assert Supervisor.child_spec(Dank.Bucket, []).restart == :temporary
  end
end
