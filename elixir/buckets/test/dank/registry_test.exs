defmodule Dank.RegistryTest do
  use ExUnit.Case, async: true

  setup context do
    _ = start_supervised!({Dank.Registry, name: context.test})
    %{registry: context.test}
  end

  test "spawns buckets", %{registry: registry} do
    assert Dank.Registry.lookup(registry, "shopping") == :error

    # Dank.Registry.create(registry, "shopping")
    # assert {:ok, bucket} = Dank.Registry.lookup(registry, "shopping")

    # Dank.Bucket.put(bucket, "milk", 1)
    # assert Dank.Bucket.get(bucket, "milk") == 1
  end

  # test "removes buckets on exit", %{registry: registry} do
  #   Dank.Registry.create(registry, "shopping")
  #   {:ok, bucket} = Dank.Registry.lookup(registry, "shopping")
  #   Agent.stop(bucket)

  #   assert Dank.Registry.lookup(registry, "shopping") == :error
  # end

  # test "removes buckets on crash", %{registry: registry} do
  #   Dank.Registry.create(registry, "shopping")
  #   {:ok, bucket} = Dank.Registry.lookup(registry, "shopping")
  #   Agent.stop(bucket, :shutdown)

  #   assert Dank.Registry.lookup(registry, "shopping") == :error
  # end
end
