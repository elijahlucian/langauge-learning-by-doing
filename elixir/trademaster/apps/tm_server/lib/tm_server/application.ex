defmodule TMServer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    port = String.to_integer(System.get_env("PORT") || "4040")

    children = [
      {Task.Supervisor, name: TMServer.TaskSupervisor},
      Supervisor.child_spec({Task, fn -> TMServer.accept(port) end}, restart: :permanent)
      # Starts a worker by calling: TMServer.Worker.start_link(arg)
      # {TMServer.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TMServer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
