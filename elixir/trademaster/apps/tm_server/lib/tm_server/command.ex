defmodule TMServer.Command do
  require Logger

  @doc """
  Runs a command
  """
  def run(command)

  def run({:create, bucket}) do
  end

  def run({:get, bucket}) do
  end

  def run({:put, bucket}) do
  end

  def run({:delete, bucket}) do
  end

  @doc ~S"""
  Parses the given `line` into a command

  ## Examples

      iex> TMServer.Command.parse("CREATE shopping \r\n")
      {:ok, {:create, "shopping"}}

      iex> TMServer.Command.parse("PUT shopping milk 3 \r\n")
      {:ok, {:put, "shopping", "milk", "3"}}

      iex> TMServer.Command.parse("GET shopping milk \r\n")
      {:ok, {:get, "shopping", "milk"}}

      iex> TMServer.Command.parse("DELETE shopping eggs \r\n")
      {:ok, {:delete, "shopping", "eggs"}}

      iex> TMServer.Command.parse("GET shopping \r\n")
      {:error, :unknown_command}

      iex> TMServer.Command.parse("BROUHAHA dank \r\n")
      {:error, :unknown_command}

  """
  def parse(line) do
    Logger.info("line #{line}")

    case String.split(line) do
      ["CREATE", bucket] -> {:ok, {:create, bucket}}
      ["PUT", bucket, item, value] -> {:ok, {:put, bucket, item, value}}
      ["GET", bucket, item] -> {:ok, {:get, bucket, item}}
      ["DELETE", bucket, item] -> {:ok, {:delete, bucket, item}}
      _ -> {:error, :unknown_command}
    end
  end
end
