import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

var count = 1;

class App extends Component {

  state = {thing: 1}

  clicked = (e, i) => {
    this.setState({thing: this.state.thing + i})
  }

  render() {
  
    count += 1

    

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">{this.state.thing}</h1>
        </header>
        <p className="App-intro">
          {count}
        </p>
        <button onClick={(e) => {this.clicked(e, 5)}}>add 5</button>
        <button onClick={(e) => {this.clicked(e, 10)}}>add 10</button>
      </div>
    );
  }
}

export default App;
