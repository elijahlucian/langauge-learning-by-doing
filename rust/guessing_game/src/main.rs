use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {
    
    let mut guess = String::new();
    let secret_number = rand::thread_rng().gen_range(1,100);
    let thing = "Number";
    let mut win = false;

    println!("Guess the number!");

    for i in 1..6 {
        println!("Try #{} / 5!", i);
        let mut guess = String::new();
        println!("Pick a {}!", thing);

        io::stdin().read_line(&mut guess)
            .expect("Failed to read line");
        
        let guess: u32 = guess.trim().parse()
            .expect("Please type a number!");
        
        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("too small"),
            Ordering::Greater => println!("too big"),
            Ordering::Equal => {
                for i in 1..5 { 
                    println!("you winnn") 
                } 
                win = true;
                break;
            },
        }

    }

    if win {
        println!("The secret number was ... {}!", secret_number);
    } else {
        println!("You Lose!");
        println!("The secret number was ... {}!", secret_number);
    }

}
