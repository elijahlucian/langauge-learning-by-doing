use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;
use std::fs;

fn main() {
		
	let host = "127.0.0.1";
	let port = "7878";

	let listener = TcpListener::bind(format!("{}:{}", host, port)).unwrap();

	println!("Web Server listening to {} on port: {}", host, port);

	for stream in listener.incoming() {
		let stream = stream.unwrap();
		
		handle_connection(stream);
	}


}

fn handle_connection(mut stream: TcpStream) {
	let mut buffer = [0; 512];

	stream.read(&mut buffer).unwrap();

    let contents = fs::read_to_string("hello.html").unwrap();

    let response = format!("HTTP/1.1 200 OK\r\n\r\n{}", contents);

    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}