type MockData = Record<keyof Thing, Record<string, any>>;

const mockData = {
  getUsers: [{ id: "asdf", name: "shit" }],
};

function hasmock() {
  console.log("hasmock");

  return function (
    target: any,
    propertyKey: string,
    descriptor: PropertyDescriptor
  ) {
    console.log(`checking env`, process.env.NODE_ENV);
    if (process.env.NODE_ENV === "development") {
      console.log(`Getting Mock Data for ${propertyKey}`);
      return (target: any) => mockData[propertyKey as keyof Thing];
    }
  };
}

class Thing {
  @hasmock()
  static async getUsers() {
    console.log("GET /api/users");
    console.log("gotten users from API!");
  }
}

Thing.getUsers();
