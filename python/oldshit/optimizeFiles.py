import os, shutil, time, datetime, subprocess
from pathlib import Path

# define all folders to be optimized, and those not - using datetime
# list subFolders
# list full path filenames to be sorted into different sections
# CR2 for raw processing
# JPG for JPEGOPTIM
# MOV to be moved to SWAP

#globals
garbagePath = "/media/swap/trash-bin" #put all the trash in a trash folder for deletion
archivePath = "/media/swap/archive"
sourcePath = "/media/kandeNAS"

thisYear = datetime.date.today().year

currentYears = [str(thisYear),str(thisYear-1),str(thisYear-2)]

yearsToKeep = (i*-1 for i in range(0,3))
print(*yearsToKeep)

iterator = 0
processedNumber = 0


#init Arrays
oldYears = []
rawFileList = []
jpgFileList = []
tifFileList = []
movieFileList = []
otherFiles = []

pathsToOptimize = []
filesToDelete = []

garbageFileList = ['db', 'psd', 'ds_store', 'dropbox', 'png', 'dng', 'zip', 'thm', 'lrcat', 'lrprev', 'cdr', 'indd', 'saproj']
deleteFileList = ['thm','lrcat','lrprev','dropbox'] #do not recommend unless absolutely sure - periods in filenames may spoof the file recognition

#Temp variables used for debug
gfType = [] # temp variable used to find new file names
rawType = "cr2"

def moveToTrash():
    for f in filesToDelete:

        shutil.move(f,)

def checkIfDirectoryExists(x):
    if not os.path.isdir(x):
        os.makedirs(x)
    return True

def convertPhotos(currentFolder, currentFileName, type):

    currentFilePath = os.path.join(sourcePath,currentFolder, currentFileName)
    optimizedFolder = os.path.join(currentFolder,"optim")
    outputFile = os.path.join(sourcePath,optimizedFolder,currentFileName[:-4]+".jpg")
    outputFileToArchive = os.path.join(archivePath,currentFolder,currentFileName[:-4]+".jpg")

    if not os.path.isdir(optimizedFolder):
        os.makedirs(optimizedFolder)
    if (type == 'raw'):
        rawType = currentFileName[-3:].lower()
        rawConvert = rawType + ":" + currentFilePath
        filesToDelete.append(currentFilePath)
        print("convert", rawConvert,"-resize","75%",outputFile)
        # subprocess.call(["convert", rawConvert,"-resize","75%%",outputFile])

    elif (type == 'jpg'):
        print("convert", currentFilePath,"-resize","75%",outputFile)
        # subprocess.call(["convert", currentFilePath,"-resize","75%%",outputFile])

    # print("jpegoptim","-m70",outputFile)
    # subprocess.call(["jpegoptim","-m70",outputFile])

    outputPathToCheck = os.path.join(archivePath,currentFolder)
    if not os.path.isdir(outputPathToCheck):
        os.makedirs(outputPathToCheck)

    # shutil.move(outputFile,outputFileToArchive)

    return True

def convertVideos(currentFilePath, currentFileName):
    # add sorting of videos for later processing...
    videoDestination = os.path.join(archivePath, 'video', currentFileName[-1][:-4] + '.mp4')
    trashBin = os.path.join(garbagePath, 'video', currentFileName[-1])
    if os.path.isfile(videoDestination):
        increment = 1
        while increment < 100:
            try:
                videoDestination = os.path.join(archivePath, 'video', currentFileName[-1][:-4] + "-"+ str(increment) + '.mp4')
                trashBin = os.path.join(garbagePath,'video', '2-' + currentFileName[-1])
                break
            except:
                increment = increment + 1
                continue

    videoSource = os.path.join(sourcePath,currentFilePath)
    videoConvertCommand = ["ffmpeg", "-i", videoSource,"-strict", "-2", "-c:v", "libx264", "-crf", "23", videoDestination]
    print("You Must Turn On Video Conversion")
    # subprocess.call(videoConvertCommand)
    # Path(videoDestination).touch()
    # shutil.move(currentFilePath,trashBin)
    return True

def throwOutTheTrash(currentFilePath,nameSource):
    fileDest = os.path.join(garbagePath,nameSource[-1])
    checkIfDirectoryExists(garbagePath)
    if not os.path.isfile(fileDest):
        print("file copied")
        shutil.move(currentFilePath, garbagePath)
    else:
        pathIterator = 1
        while pathIterator < 100:
            testPath = os.path.join(garbagePath,str(pathIterator))
            try:
                checkIfDirectoryExists(testPath)
                print("file copied to subdir", testPath)
                shutil.move(currentFilePath, testPath)
                break
            except:
                pathIterator = pathIterator + 1
                continue
    return True

def checkForSubfolders(currentFolder, listOfSubFolders):
    for itemCheck in listOfSubFolders:
        currentCheck = os.path.join(currentFolder,itemCheck)
        if os.path.isdir(currentCheck):
            pathsToOptimize.append(currentFolder)
            subFolderList = os.listdir(currentCheck)
            checkForSubfolders(currentCheck,subFolderList)
        else:
            sortFilesInFolder(currentCheck,currentFolder)
    return True

def sortFilesInFolder(currentFilePath,currentFolder):
    currentFileName = currentFilePath.split('/')
    typeSource = currentFileName[-1].split('.')
    fileType = typeSource[-1].lower()
    if (fileType == "cr2") or (fileType == "nef") or (fileType == "arw"):
        convertPhotos(currentFolder,currentFileName[-1],"raw")
        rawFileList.append(currentFilePath)
    elif fileType == "jpg":
        convertPhotos(currentFolder,currentFileName[-1],"jpg")
        jpgFileList.append(currentFilePath)
    elif (fileType == "mov") or (fileType == "mp4") or (fileType == "mts"):
        convertVideos(currentFilePath, currentFileName)
        movieFileList.append(currentFilePath)
    elif (fileType == "tif") or (fileType == "tiff"):
        tifFileList.append(currentFilePath)
    elif fileType in garbageFileList:
        throwOutTheTrash(currentFilePath,nameSource)
    else:
        otherFiles.append(currentFilePath)
        fileStat = os.stat(currentFilePath)
        print("unrecognized file type. outputting in list at bottom of program.")
        if not fileType in gfType:
            gfType.append(fileType)

    return True

def getRootDirectories(dir):
    for f in dir:
        if "20" in f:
            year = f
            if not year in currentYears:
                oldYears.append(f)
    return oldYears.sort()

oldyears = getRootDirectories(os.listdir())

print("\nWelcome To Optimization!\n")
print("don't use these folders",currentYears)
print("optimize these ones", oldYears)

allYears = oldYears # + currentYears # for debugging
allYears.sort()

for currentFolder in allYears:
    listOfSubFolders = os.listdir(currentFolder) #returns list of subfolders
    checkForSubfolders(currentFolder, listOfSubFolders) #check for subfolders or sort files
    iterator = iterator + 1

print("\n\n")

print (len(pathsToOptimize), "paths to be optimized!")

mb = len(rawFileList * 30)
print(len(rawFileList),"raw files to be processed - total of ",int(mb/1024),"GB to optimize")

mb = len(jpgFileList * 7)
print(len(jpgFileList), "jpg files to be processed - total of ",int(mb/1024),"GB to optimize")
print(len(tifFileList), "tif files to be processed")

print(len(movieFileList), "video files and",len(otherFiles), "other file types to be processed")

print("garbage file types include:", *(i for i in gfType) )

print("\n\n")
