import os, shutil, time, datetime

# define all folders to be optimized, and those not - using datetime
# list subFolders
# list full path filenames to be sorted into different sections
# CR2 for raw processing
# JPG for JPEGOPTIM
# MOV to be moved to SWAP

#init Arrays
oldYears = []
rawFileList = []
jpgFileList = []
tifFileList = []
movFileList = []
otherFiles = []
pathsToOptimize = []

#globals
thisYear = datetime.date.today().year
currentYears = [str(thisYear),str(thisYear-1),str(thisYear-2)]
iterator = 0

def checkForSubdirectories(dir):
    return 1

def getRootDirectories(dir):
    for f in dir:
        if "20" in f:
            year = f
            if not year in currentYears:
                oldYears.append(f)
    return oldYears.sort()

oldyears = getRootDirectories(os.listdir())

print("\nWelcome To Optimization!\n")
print("don't use these folders",currentYears)
print("optimize these ones", oldYears)

allYears = oldYears + currentYears#for debugging - insert currentYears
allYears.sort()

for i in allYears:
    subFolders = os.listdir(i)

    #recurse this shit
    for j in subFolders:
        k = os.path.join(allYears[iterator],j)
        pathsToOptimize.append(k)
        fileList = os.listdir(k)
        for fileName in fileList:
            filePath = os.path.join(k,fileName)

            if os.path.isdir(filePath):
                print(filePath,"is a directory")
                continue
            fileType = fileName[-3:].lower()
            if fileType == "cr2":
                rawFileList.append(filePath)
            elif fileType == "nef":
                rawFileList.append(filePath)
            elif fileType == "jpg":
                jpgFileList.append(filePath)
            elif fileType == "mov":
                movFileList.append(filePath)
            elif fileType == "tif":
                tifFileList.append(filePath)
            else:
                otherFiles.append(filePath)
                fileStat = os.stat(filePath)
                print(filePath, "size: ",int(fileStat.st_size/1048576),"MB" )
            # print(filePath)
    iterator = iterator + 1

mb = len(rawFileList * 30)
print(len(rawFileList),"raw files to be processed - total of ",int(mb/1024),"GB to optimize")

mb = len(jpgFileList * 7)
print(len(jpgFileList), "jpg files to be processed - total of ",int(mb/1024),"GB to optimize")

print(len(movFileList), "mov files and",len(otherFiles), "other file types to be processed")
#
# print("\nFolders To Be Culled")
# os.path.join(i)
# subFolders = os.listdir(oldYears[0])
# print(subFolders)
#
# try:
#     files = os.listdir(oldYears[0])
# except:
#     print("no subfolders")
