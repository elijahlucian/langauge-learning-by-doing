import sys

available_commands = ['sayhi','bark','meow']
another_list = ['kaela','toffee','elijah']

multiples = 1
command = sys.argv[1]
try:
    multiples = int(sys.argv[2])
except ValueError:
    print("must put an int for second variable")
    quit()

confirmed_command = [letter for letter in available_commands if letter in command] * multiples

if not confirmed_command:
    print("command not found")
else:
    print(confirmed_command)

for action in available_commands:
    for noun in another_list:
        print('the',noun,action+'s at sunrise')

x = [(action,noun) for action in available_commands for noun in another_list]
for (y,z) in x:
    print(y)
