squares = [x ** 2 for x in range(1,11)]

def f(x):
    if x > 30 and x < 70:
        return True
    else:
        return False

for n in squares:
    print(f(n))

print(list(filter(f,squares)))

x = filter(lambda x: f(x), squares)
print(list(x))
