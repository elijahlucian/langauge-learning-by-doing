import math
import random
import time
import datetime

start = int(time.time())

r = 100000
avg, greater, lesser = 0,0,0

def doTheGauss(avg,greater,lesser):

    seed = int(round(time.time() * 1000))
    random.seed(seed)

    for i in range(r):
        x = random.randint(0,1)
        swap = avg
        avg = x + swap

    gauss = avg / r

    if gauss > 0.5:
        greater = greater + 1
    else:
        lesser = lesser + 1

    return(greater,lesser)

for i in range(r):
    greater,lesser = doTheGauss(avg,greater,lesser)

end = round(time.time(),2)

print("Script Time:", round(end - start,2), "seconds elapsed!")

print("greater numbers in sampling:",greater)
print("lesser numbers in sampling:",lesser)
print()
if greater > lesser:
    "Gauss Was Right!"
else:
    "Gauss was an idiot!"
