import random

x = []

square_size = 5

for i in range(0,square_size):
    x.append([])
    x[i] = []
    for j in range(0,square_size):
        x[i].append(random.randint(0,9))

for i in x:
    for j in i:
        print(j, end=' ')

    print()

# print(x)
