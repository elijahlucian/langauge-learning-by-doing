from flask import Flask, render_template
app = Flask(__name__)

x = 0

@app.route('/')
def index(page_name):
    return render_template('./index.html')

@app.route('/api')
def api():
    global x
    if x == 0:
        x = 1
    else:
        x = 0
    return str(x)
