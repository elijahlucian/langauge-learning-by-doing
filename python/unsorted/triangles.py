# triangles.py

def t(x,y,z):
    if x == y:
        if x == z:
            return 'EQUILATERIAL'
        else: return 'ISOSCELES'
    else: return 'SCALENE'

print(t(2,3,4))
print(t(6,6,6))
print(t(3,6,3))
print(t(6,4,4))
