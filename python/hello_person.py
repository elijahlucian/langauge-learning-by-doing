print("Heyo! Please enter your name!")  # this is an intro message, it lets the user know what is happening
user_name = input()  # this is actually what triggers user input
print("Hello {}!!".format(user_name))  # this prints out "Hello" and then the user name
