import time
import os, zipfile, xml
import xml.etree.ElementTree as ET

def getfile():
    scriptfile = 'scripts/santa.xml'

    # scriptfile = zipfile('someshit')

    tree = ET.parse(scriptfile)
    root = tree.getroot()

    for line in root:
        print(line)

    return root

def main(root = getfile()):
    # root = getfile()

    tag_to_find = 'text'
    tag_list = list(root.iter(tag_to_find))

    for tag in tag_list:
        print(tag.attrib, tag.text)

if __name__ == '__main__':
    main()
