import numpy as np
import scipy.io.wavfile as wavfile

audio_file = 'claps.wav'

rate, data = wavfile.read(audio_file)

cooldown = False
cooldown_counter = 0
cooldown_length = 1000

for index, byte in enumerate(data):
    if cooldown == True:
        
        if cooldown_counter > cooldown_length:
            cooldown_counter = 0
            print("cooldown reached")
            cooldown = False
        else:
            cooldown_counter += 1
    else:
        if byte[0] > 0.5: 		
            print("Byte value %s at index %s!" % (byte[0], index) )
            cooldown = True

sin_data = np.sin(data)

print(sin_data)