
import sys
from PyQt5.QtWidgets import QMainWindow, QAction, qApp, QApplication
from PyQt5.QtGui import QIcon


class Example(QMainWindow):

    def __init__(self):
        super().__init__()

        self.initUI()


    def initUI(self):

        exitAct = QAction(QIcon('exit.png'), '&Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(qApp.quit)

        tehPronz = QAction(QIcon('exit.png'), 'Get Teh &Pronz', self)
        tehPronz.setShortcut('Ctrl+P')
        tehPronz.setStatusTip('Open All Teh Pronz')

        delPronz = QAction(QIcon('exit.png'), '&Delete Teh Pronz', self)
        delPronz.setShortcut('Ctrl+D')
        delPronz.setStatusTip('Delet All Teh Pronz (plz don\'t)')

        self.statusBar()

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAct)

        pronMenu = menubar.addMenu('&Pron')
        pronMenu.addAction(tehPronz)
        pronMenu.addAction(delPronz)

        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Simple menu')
        self.show()


if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
