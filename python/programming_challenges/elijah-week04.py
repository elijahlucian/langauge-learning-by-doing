logo = [
"\n _____ ________  ___ ___  _   _ _____ _____ _____  _____   ",
"\n/  ___|  ___|  \/  |/ _ \| \ | |_   _|_   _/  __ \/  ___|  ",
"\n\ `--.| |__ | .  . / /_\ \  \| | | |   | | | /  \/\ `--.   ",
"\n `--. \  __|| |\/| |  _  | . ` | | |   | | | |     `--. \  ",
"\n/\__/ / |___| |  | | | | | |\  | | |  _| |_| \__/\/\__/ /  ",
"\n\____/\____/\_|  |_|_| |_|_| \_/ \_/  \___/ \____/\____/   ",
"\n                                                           ",
"\n                 __ ______ _____ _____  __                 ",
"\n       ______   / / | ___ \  _  |_   _| \ \   ______       ",
"\n _____|______| / /  | |_/ / | | | | |    \ \ |______|_____ ",
"\n|______|_____ / /   | ___ \ | | | | |     \ \ _____|______|",
"\n      |______/ /    | |_/ | \_/ / | |      \ \______|      ",
"\n            /_/     \____/ \___/  \_/       \_\            ",
"\n                                                           ",
"\n      ________  _____ _____ _____ _____   ________         ",
"\n _____\ \ \ \ \|  ___|  _  |  _  |  _  | / / / / /_____    ",
"\n|______\ \ \ \ \___ \| |/' | |/' | |/' |/ / / / /______|   ",
"\n ______ > > > > >  \ \  /| |  /| |  /| < < < < < ______    ",
"\n|______/ / / / /\__/ | |_/ | |_/ | |_/ /\ \ \ \ \______|   ",
"\n      /_/_/_/_/\____/ \___/ \___/ \___/  \_\_\_\_\         ",
"\n                                                           ",
"\n                      v1.0 ELI7VH                          ",
"\n                                                           "
]
import time

char = ""

def wrong(n):
  if n == 2:
    msg(["it seems you're not in an answering mood...","so...","see ya!"])
    quit()
  else:
    msg(["\nAnswer the god damned question!\n"])
    n += 1
    return n

def msg(m, speed="slow"):
  print()
  sleep = 0.03
  if speed == "fast":
      sleep = 0.005

  for w in m:
    for l in w:

        print(l,end="",flush="True")
        if speed == "fast":
            pass
        elif speed == "slow":
            time.sleep(sleep)

    print(" ",end="",flush="True")
    time.sleep(sleep*20)

  print()

def snake_case():
  print_statement = ["did you know that snake_case is the community-accepted way to name your variable and functions?","for example this function is named 'snake_case()' and the variable that this print statement is in is called 'print_statement'. While Elijah does not prefer this convention, and uses his own way, the 'correct' way is snake_case"]
  msg(print_statement)
  return True

def camelCase():
  printStaement = ["in the words of the python community, camelCase is not a proper naming convention while using python.","Elijah thinks this is incrediblyStupid as camelCase is significantlyFasterToType.","The name of this function is called 'camelCase()' and the variable that this text is stored in is called 'printStaement'"]
  msg(printStaement)

msg(logo,"fast")

didntAnswer = 0
msg(["Do you prefer to name your variables with snake_case?","or do you like the elegance of camelCase?"])

while True:
  # print(didntAnswer)

  user = input(": ")

  try: #check if the user even put something in
    x = user[0]
    if x == "s":
      snake_case()
      break

    elif x == "c":
      camelCase()
      break

    else:
      didntAnswer = wrong(didntAnswer)
      continue

  except IndexError:
    didntAnswer = wrong(didntAnswer)
    continue


  # else:
  #   didntAnswer = wrong(didntAnswer)
  #   continue

msg(["thanks for using SemanticsBot v1.0!","\nFeeling fighty?","\nJust use SemanticsBot!"])
