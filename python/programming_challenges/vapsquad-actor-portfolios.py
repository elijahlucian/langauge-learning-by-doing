import time

def msg(message_list, slow=True, message_type='text'): # ELI7VH's magical message method v1.2
    # input ['some','list'] to continue. can also be a ['single element list']

    # print()

    sleep = 0.005 if slow == False else 0.03

    for element in message_list:
        if message_type == "list":
          print("\t", end="")

        for letter in element:
            print(letter, end='', flush='True')

            if slow==True:
                time.sleep(sleep)

        if message_type == 'logo':
            print(' ', flush='True')
        else:
            print(' ', end='', flush='True')

        time.sleep(sleep*20)

    print()

class Actor:
    def __init__(self, name):
        self.name = name
        self.game_list = []
        self.book_list = []

    def add_game(self,*game):
        for game_name in game:
            self.game_list.append(game_name)

    def add_book(self,*book):
        for book_name in book:
            self.book_list.append(book_name)

    def list_title(self,list_type):
        msg(["\n",self.name,list_type,"List:\n\n"])

    def list_published_works(self):

        self.list_title("Game")
        for game in self.game_list:
            msg([game], message_type = "list")

        self.list_title("Book")
        for book in self.book_list:
            msg([book], message_type = "list")

randolph = Actor("Randolph West")
randolph.add_game("Unknown Fate (Steam)", "Omen  Of Sorrow (PS4)", "SupraBall (Steam)", "Orcs (Mobile)", "Stage Presence (Steam)", "Jupiter Omega (Steam)", "Pik And Pok", "Blade And Bones (PS4)", "Clumsy Moose Season (Steam)","Blameless","Project Kingdom")
randolph.add_book("David Wing - My Chest", "Jane Risdon - Apartment 206c", "David Wing - The Pugilist")
randolph.list_published_works()

victoria = Actor("Victoria Marie")
victoria.add_game("Blameless")
victoria.add_book("")

print()
