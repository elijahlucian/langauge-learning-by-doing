def msg(message_list, slow=True, message_type='text'): # ELI7VH's magical message method v1.2
    # input ['some','list'] to continue. can also be a ['single element list']

    print()

    sleep = 0.005 if slow == False else 0.03

    for element in message_list:
        for letter in element:
            print(letter, end='', flush='True')

            if slow==True:
                time.sleep(sleep)

        if message_type == 'logo':
            print(' ', flush='True')
        else:
            print(' ', end='', flush='True')

        time.sleep(sleep*20)

    print()
