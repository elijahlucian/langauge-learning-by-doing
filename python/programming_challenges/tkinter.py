import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QPushButton, QToolTip, QMessageBox)
from PyQt5.QtGui import  QFont

class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        QToolTip.setFont(QFont('monofur', 16))
        self.setToolTip('This is a thingy onthe screen!!')

        btn = QPushButton('Button', self)

        btn.clicked.connect(self.setWindowTitle('Is A Hottie'))

        btn.setToolTip('Press Meh!!')
        btn.resize(btn.sizeHint())
        btn.move(50,50)

        self.setGeometry(300,300,300,220)
        self.setWindowTitle('Your Mom')
        self.show()

    def popText(self, event):
        reply = QMessageBox.question(self, 'Message',
            "Are you sure to quit?", QMessageBox.Yes |
            QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore() 

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
