import time, readchar, random, sys

class Loot:

    possibleLoot = ["Shiny Gems","Sticky Boogers","Golden Pocket Lint","Strange Goo"]
    possibleValue = []

    def __init__(self,name):
        self.name = name
        self.value = random.randint(1,1000)

class Player:
    def __init__(self):
        self.backpack = []
    def equipWeapon(self, weapon):
        self.weapon = weapon
    def equipArmor(self, armor):
        self.armor = armor
    def hit(self, monster):
        self.weapon.hit(monster)

class Monster:

    attacks = 20 # all monsters will have this base attack value

    def __init__(self, name, armor, hp = 100, specialAttack = 30):
        self.name = name
        self.hp = hp
        self.armor = armor
        self.specialAttack = specialAttack
        self.alive = True

    def lootMonster():
        if self.alive == True:
            print("You Can't loot",self.name,"while it's still breathing!")

    def successfulHit(self):
        time.sleep(0.5)
        print("HIT!")
        time.sleep(0.5)

    def takeDamage(self, weapon):
        writeMessage("...... ") # check hit chance
        self.successfulHit()
        if self.armor >= weapon.dmg:
            self.armor = self.armor - weapon.dmg / 2
        elif self.armor > 0:
            print(self.name+"'s armor has been broken!")
            self.armor = 0
        elif self.hp <= weapon.dmg:
            print(self.name,"has been vanquished!")
            self.hp = 0
            self.alive = False
        else:
            self.hp = self.hp - weapon.dmg

class Weapon:
    def __init__(self, name, dmg, dur):
        self.name = name
        self.dmg = dmg
        self.dur = dur

    def hit(self, monster):
        monster.takeDamage(self)
        self.dur = self.dur - monster.armor/10

sabre = Weapon("Crystal Sabre", 12,100)
ghoul = Monster("Rabid Ghoul",20)
player = Player() # init player
player.equipWeapon(sabre)

def writeMessage(*msg):
    print()
    for word in msg:
        word = str(word)
        for letter in word:
            print(letter,end="",flush=True)
            time.sleep(.02)
        print(" ",end="")
    print()

def attack(target, player):

 # add a cool bit of tension here.
    player.weapon.hit(target)
    if target.hp == 0:
        print(target.name,"is dead!")
        return False

    writeMessage("Hitting",target.name,"with",player.weapon.name)
    writeMessage(target.name," has HP:",target.hp, "and AP:",target.armor)
    writeMessage("Your",player.weapon.name,"has",player.weapon.dur,"durability")

    time.sleep(1)
    return True

while True:
    print("Do you want to [a] Attack! [d] Defend [r] Run Away!:")
    choice = readchar.readchar()
    target = ghoul # get player to choose from targets

    if choice == 'a':
        attack(target, player)
    elif choice == 'd':
        writeMessage("Defending")
    elif choice == 'r':
        writeMessage("Running away like a little bitch!")
        quit()
