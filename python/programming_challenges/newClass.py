import time

class Inventory:
    def __init__(self):
        self.backpack = []

    def equipWeapon(self, weapon):
        self.weapon = weapon

    def equipArmor(self, armor):
        self.armor = armor

    def hit(self, monster):
        self.weapon.hit(monster)

class Monster:
    def __init__(self, name, armor, hp = 100):
        self.name = name
        self.hp = hp
        self.armor = armor
        self.alive = True

    def takeDamage(self, weapon):
        if self.armor >= weapon.dmg:
            self.armor = self.armor - weapon.dmg / 2
        elif self.armor > 0:
            self.armor = 0
        elif self.hp <= weapon.dmg:
            self.alive = False
        else:
            self.hp = self.hp - weapon.dmg

class Weapon:
    def __init__(self, name, dmg, dur):
        self.name = name
        self.dmg = dmg
        self.dur = dur

    def hit(self, monster):
        monster.takeDamage(self)
        self.dur = self.dur - monster.armor/10

sabre = Weapon("Crystal Sabre", 12,100)
ghoul = Monster("Rabid Ghoul",20)
inventory = Inventory() # init inventory
inventory.equipWeapon(sabre)

def attack(monster, inventory):
    inventory.hit(monster)

for x in range(10):
    attack(ghoul, inventory)
    print()
    print("Hitting",ghoul.name,"with",inventory.weapon.name)
    print(ghoul.name," has HP:",ghoul.hp, "and AP:",ghoul.armor)
    print("Your",inventory.weapon.name,"has",inventory.weapon.dur,"durability")
    time.sleep(1)
