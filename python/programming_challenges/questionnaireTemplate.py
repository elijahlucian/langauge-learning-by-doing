class QA(object):
    def __init__(self, question, choices):
        self.question = question
        self.choices = choices
        self.answer = None

    def setAnswer(self, answer):
        while not answer in self.choices:
            answer = input(self.question + " ")
        self.answer = answer

qa = [
    QA("do you like peanut butter?",["yes", "no"]),
    QA("where do you put peanut butter?",["on bread", "on my dog", "in the garbage"])
]

for i,q in enumerate(qa):
    # ask question
    print(q[0])

    answersRange = range(len(q[1])) # get number of answers

    for k,a in enumerate(q[1]): # display all answers
      print(k,a)

    while True:
        while True:
            try:
                x = int(input(": "))
                break
            except ValueError:
                print("That's not an integer")
        if x in answersRange:
            q[2] = x
            break
        print("invalid answer")


qa = [
        {
            "question": "do you like peanut butter?",
            "choices": ["on bread", "on my dog"],
            "answer": ""
        },
        {
            "question": "where do you put peanut butter?",
            "choices": ["on bread", "on my dog", "in the garbage"],
            "answer": ""
        }
]
