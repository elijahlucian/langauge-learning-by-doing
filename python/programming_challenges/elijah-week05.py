logo = [
"       ___ ___    _____  ____  ___________ __________        ",
"      /   |   \  /  _  \ \   \/  /\_____  \\______   \ ",
"     /    ~    \/  /_\  \ \     /  /   |   \|       _/ ",
"     \    Y    /    |    \/     \ /    |    \    |   \ ",
"   /\ \___|_  /\____|__  /___/\  \\_______  /____|_  / /\ ",
"   \/       \/         \/      \_/        \/       \/  \/ ",
"  /\            __________ ___________________           /\ ",
"  \ \           \______   \\_____  \__    ___/          / / ",
"   \ \    ______ |    |  _/ /   |   \|    |  ______    / / ",
"    \ \  /_____/ |    |   \/    |    \    | /_____/   / / ",
"     \ \         |______  /\_______  /____|          / / ",
"      \/                \/         \/                \/ ",
"           ________  __      _________  __________ ",
"           \_____  \/  \    /  \      \ \____    / ",
"    ______  /   |   \   \/\/   /   |   \  /     /   ______ ",
"   /_____/ /    |    \        /    |    \/     /_  /_____/ ",
"           \_______  /\__/\  /\____|__  /_______ \ ",
"                   \/      \/         \/        \/ ",
"       /\      /\ _____.___.________   ____ ___/\/\ ",
"      / /     / / \__  |   |\_____  \ |    |   \ \ \ ",
"     / /     / /   /   |   | /   |   \|    |   /\ \ \ ",
"    / /     / /    \____   |/    |    \    |  /  \ \ \ ",
"   / /     / /     / ______|\_______  /______/    \ \ \ ",
"   \/      \/      \/               \/             \/\/ ",
" ",
]

import time, replit

start = time.time()
end = start
replit.clear()

def msg(m, speed='slow', type='text'): # ELI7VH's magical message method

  # input ['some','list'] to continue. can also be a ['single element']

  print()
  sleep = 0.03
  if speed == 'fast':
      sleep = 0.005

  for w in m:
    for l in w:

        print(l,end='',flush='True')
        if speed == 'fast':
            pass
        elif speed == 'slow':
            time.sleep(sleep)
    if type == 'logo':
      print(' ',flush='True')
    else:
      print(' ',end='',flush='True')
    time.sleep(sleep*20)

  print()

msg(logo,"fast","logo")

while True:
  msg(["You has been haxord. go to www.playpal.com and input your credit card 4 fix."])
  time.sleep(0.15)
  end = time.time()
  if end-start >= 10:
    break

replit.clear()
msg(['.','.','.','.','.','.','.',])
msg(['jk fixed!'])
