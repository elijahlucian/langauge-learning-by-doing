# databaseify this.

roster = {
  'elijah': {
    'handle': '@eli7vh',
    'tagline': 'The Diggity Dankest',
    'color': '#ff69b4',
  },
  'randolph': {
    'handle': '@randolph',
    'tagline': 'Officially disguised as a Canadian',
    'color': '#80a4ed',
  },
  'shylo': {
    'handle': '@ltsnakeplissken',
    'tagline': 'Definitely exists and is alive',
    'color': '#000080',
  },
  'nancy': {
    'handle': '@exdevlin',
    'tagline': 'Unassuming tiny rage',
    'color': '#771f1f'
  },
  'nat': {
    'handle': '@flippychips',
    'tagline': 'Fellow of infinite jest',
      'color': '#df5286'
  },
    'eric': {
      'handle': 'cytokinesis',
      'tagline': 'Does he have free time?!',
      'color': 'fe5a1d'
    }
}

