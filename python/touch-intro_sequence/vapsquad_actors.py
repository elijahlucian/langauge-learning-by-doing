in_the_house = ['elijah', 'shylo', 'randolph']

roster = {}

def load_text(handle, tagline):
	# name_label.text = handle
	# tagline_label.text = tagline
	pass

for actor in in_the_house:
	
	if actor not in roster.keys():
		target_text = op('error_log')
		target_text.par.text = "{} is not registered in the roster!".format(actor)
		continue

	handle = actor_info['handle']
	tagline = actor_info['tagline']
	load_text(handle, tagline)

