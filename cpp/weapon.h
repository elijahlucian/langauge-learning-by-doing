#include <iostream>

class Weapon {
  public:
    Weapon(int dam, int dur, int price);
    void Swing();
    void SetStats(int dam, int dur, int price);
    int GetDamage();
    int GetDurability();
    int GetPrice();

  private:
    int wDamage;
    int wDurability;
    int wPrice;
};
