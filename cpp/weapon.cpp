#include "weapon.h"

Weapon::Weapon(int dam = 10, int dur = 20, int price = 30) {
  wDamage = dam;
  wDurability = dur;
  wPrice = price;
}

int Weapon::GetDamage() {
  return wDamage;
}

int Weapon::GetDurability() {
  return wDurability;
}

int Weapon::GetPrice() {
  return wPrice;
}

void Weapon::Swing() {

}

void Weapon::SetStats(int dam, int dur, int price) {
  wDamage = dam;
  wDurability = dur;
  wPrice = price;
}
