// binary_tree.js

class Tree {
  constructor(root) {
    this.root = null
  }

  addNode(value) {
    if(this.root == null) {
      this.root = new Node(value)
    } else {
      this.root.add(value)
    }
  }

  findNode(value) {
    return this.root.search(value)
  }

  addABunchOfRandos(n) {
    for (let i = 0; i < n; i++) {
      this.addNode(Math.floor(Math.random() * 100))
    }
  }

  getIds() {
    // iterate over trees
  }
}

class Node {
  constructor(value) {
    this.left = null
    this.right = null
    this.value = value
  }

  assignValue(value, side) {
    this[side] = new Node(value)
  }

  add(value) {
    if(value == this.value) {
      console.log("This value is already stored...")
      return
    }
    if(value < this.value) {
      if(this.left) {
        this.left.add(value)
      } else {
        this.assignValue(value, 'left')
      }
    } else {
      if(this.right) {
        this.right.add(value)
      } else {
        this.assignValue(value, 'right')
      }
    }
  }

  search(value) {
    if(value == this.value) {
      this.printSelf()
      return this
    }

    if(value < this.value && this.left) {
      return this.left.search(value)
    } else if(value > this.value && this.right) {
      return this.right.search(value)
    }
    console.log("returning false")
    return false
  }

  printSelf() {
    console.log(this.value)
  }

}

const setup = () => {
  var tree = new Tree()
  tree.addNode(5)
  tree.addNode(19)
  tree.addNode(12)
  let val = 19
  let found = tree.findNode(val)
  if(found) {
    console.log("found", val)
  } else {
    console.log("not found", val)
  }
}

setup()