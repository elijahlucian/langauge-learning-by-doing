
function init() {
  window.requestAnimationFrame(draw);
}

function draw() {
  var canvas = document.getElementById('canvas')
  var ctx = canvas.getContext('2d')

  var x = 25

  ctx.fillRect(x, 25, 100, 100);

  x += 1

  window.requestAnimationFrame(draw);
}

init()
