var lightIntensity = 0.05
var note_amount = 127 // number of midi keys

var scene = new THREE.Scene()
var camera = new THREE.PerspectiveCamera( 90, window.innerWidth / (window.innerHeight-20), 0.1, 2000 )
camera.position.z = 50
var renderer = new THREE.WebGLRenderer()

var boxyboard = [] // 37 keys ( find rule for black keys )

// make boxyboard

var drumkit = [] // kick, snare, crash, hats, percs
var strings = [] // various ambient tones maybe

renderer.setSize( window.innerWidth, window.innerHeight-20 )
document.body.appendChild( renderer.domElement )

var light = new THREE.AmbientLight( 0x404040 ); // soft white light
scene.add( light );

var directionalLight = new THREE.DirectionalLight( 0xffffff, lightIntensity )
directionalLight.position.set( 0, -70, 100 ).normalize();

scene.add( directionalLight )

var geometry = new THREE.BoxGeometry( 1, 5, 1 ) 
var cubeColor = new THREE.MeshPhongMaterial({ color: 0xcccccc})
var cube = new THREE.Mesh( geometry, cubeColor )

scene.add( cube )

for (let i = 0; i < note_amount; i++) {
    
    var boxy = new THREE.BoxGeometry( 1, 1, 3 )
    // var boxyColor = new THREE.MeshPhongMaterial({color: 0x427aa1})
    var boxyKey = new THREE.Mesh( boxy, cubeColor )
    boxyKey.position.y = -30
    boxyKey.position.x = (i - 64) * 2
    boxyboard.push( boxyKey )
    scene.add( boxyKey )

}



var animate = () => {
    requestAnimationFrame( animate )
    // cube.rotation.x += 0.01
    // cube.rotation.y += 0.01
    if (lightIntensity > 0.001) {
        lightIntensity -= 0.006
    }
    
    light.intensity = lightIntensity
    directionalLight.intensity = lightIntensity
    // console.log(cube)
    renderer.render( scene, camera )
}


animate()