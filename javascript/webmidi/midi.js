var midi, data;
var rgb = {}
var mid = {}
var drums = {}
var col = document.getElementById('hello')
var notes = 0

// request MIDI access
if (navigator.requestMIDIAccess) {
    navigator.requestMIDIAccess({
        sysex: false
    }).then(onMIDISuccess, onMIDIFailure);
} else {
    alert("No MIDI support in your browser.");
}

// midi functions
function onMIDISuccess(midiAccess) {
    // when we get a succesful response, run this code
    midi = midiAccess; // this is our raw MIDI data, inputs, outputs, and sysex status

    var inputs = midi.inputs.values();
    // loop over all available inputs and listen for any MIDI input
    for (var input = inputs.next(); input && !input.done; input = inputs.next()) {
        // each time there is a midi message call the onMIDIMessage function
        input.value.onmidimessage = onMIDIMessage;
    }
}

function onMIDIFailure(error) {
    // when we get a failed response, run this code
    console.log("No access to MIDI devices or your browser doesn't support WebMIDI API. Please use WebMIDIAPIShim " + error);
}

const dx7 = (data) => {
    if (data.length > 1) {
        
        let last_pitch = mid.pitch
        mid.pitch = ((data[1]-48)/(80-48))
        
        // if (last_pitch > mid.pitch) {mid.pitch = last_pitch}

        rgb.r = Math.abs(data[1]+data[2]-68)/(223-68)*255, 
        rgb.g = mid.pitch*255
        rgb.b = (data[2]/127)*255
        

        // R 68 = 0, 223 = 255
        // G 48 = 0, 96 = 255
        // B 20 = 0, 127 = 255
        // console.log('MIDI data', data,rgb); // MIDI data [144, 63, 73]
        if (data[2] === 0) {
            notes -= 1
            if (notes === 0) {
                // col.style.color='#FFFFFF'   
                // col.style.background=`rgb(${255-rgb.r},${rgb.g},${255-rgb.b})`
                // col.style.width='30%'
                // col.style.border='0'
            }
        } else {

            let bk = boxyboard[data[1]] // key position of note
            console.log(bk)
            // bk.boxyColor = {r:255,g:255,b:255}

            lightIntensity = 0.05
            cubeColor.color.r = Math.abs(data[1]+data[2]-68)
            cubeColor.color.g = ((data[1]-48)/(96-48))*255
            cubeColor.color.b = (data[2]/127)*255
            // cube.position.z = mid.pitch * 40 - 40
            if(cube.position.x > 20) {cube.position.x = -20}
            cube.position.x = (data[1] - 64) * 2
            cube.position.y = -25
            // cube.position.y = mid.pitch * 10 - Math.abs(cube.position.x)/2
            // console.log(cube.position)
            notes += 1

            geometry.vertices.push( new THREE.Vector3( cubeColor.color.r, cubeColor.color.g/2, cubeColor.color.b/4 ))
            // console.log(geometry.vertices);
            
            // col.style.color=`rgb(${rgb.g},${rgb.r},${rgb.b})`
            // col.style.background='#FFFFFF'
            // col.style.width='38%'
            // col.style.border=`1px solid rgb(${255-rgb.r},${rgb.g},${255-rgb.b})`
            
        }
    }

}


function onMIDIMessage(message) {
    data = message.data; // this gives us our [command/channel, note, velocity] data.
    
    switch (data[0]) {
        case 153:
            drums.kick
            drums.snare
            drums.hats
            drums.crash
            drums.percs
            // drum machine
            break;
        case 145:
            // sequencer 2
            break;
        case 144:
            dx7(data)
            // sequencer 3 + yamaha dx
            break
        default:
            break;
    }

}